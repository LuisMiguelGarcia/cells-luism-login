{
  const {
    html,
  } = Polymer;
  /**
    `<luism-login>` Description.

    Example:

    ```html
    <luism-login></luism-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --luism-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class LuismLogin extends Polymer.Element {

    static get is() {
      return 'luism-login';
    }

    static get template() {
      return html `
      <style include="luism-login-styles luism-login-shared-styles"></style>
      <slot></slot>
    
      <body>
      <div id="contenido">
          <table class="tbl_tabla" >
            <tr>
              <td><h1 style="color: #fff; text-align: center;">Login</h1></td>
            </tr>
            <tr>
              <td><strong><label style="color: #fff">Usuario:</label></strong></td>
            </tr>

            <tr>
              <td><input type="text" placeholder="User" class="input_text" value="{{txtuser::input}}"></td>
            </tr>

            <tr>
              <td><strong><label style="color: #fff">Password:</label></strong></td>
            </tr>

            <tr>
              <td><input type="password" placeholder="Password" class="input_text" value="{{txtpass::input}}"></td>
            </tr>

            <tr>
              <td><button  on-click="validar" class="input_button" >Ingresar</button></td>
            </tr>
          </table>
      </div>
    </body>
      `;
    }

    static get properties() {
      return {
        txtuser: {
          type: String,
          value: ''
        },

        txtpass: {
          type: String,
          value: ''
        }
      };
    }

    validar(e) {
      if (this.txtuser === 'luis' &&  this.txtpass === 'l123') {
        this.dispatchEvent(new CustomEvent('login-sucess', { detail: this.txtuser }));//Se dispara un evento donde se envia informacion dentro del ditail
        //this.set('prop1', true);
        console.log(this.prop1);
        alert('Acceso consedido');
      } else {
        this.dispatchEvent(new CustomEvent('login-error', { detail: this.txtuser }));//Se dispara un evento donde se envia informacion dentro del ditail
        alert('Acceso denegado');
      }
    }
  }
  customElements.define(LuismLogin.is, LuismLogin);
}